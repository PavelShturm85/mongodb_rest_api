FROM pypy:3.5-7.0

WORKDIR /mongodb_rest_api

RUN mkdir mongodb_rest_api

ADD mongodb_rest_api /mongodb_rest_api/mongodb_rest_api

RUN apt-get update && apt-get upgrade -y && apt-get install -y  \
    --no-install-recommends apt-utils \
    build-essential libssl-dev libffi-dev python3-dev

RUN mkdir /project_tmp

ADD /conf/requirements/req_pypy3.txt /project_tmp/requirements.txt
ADD /conf/gunicorn/gunicorn_tornado_conf.py /project_tmp/gunicorn_conf.py
ADD /conf/project_settings/local_settings.py /mongodb_rest_api/mongodb_rest_api/local_settings.py
RUN pip install --upgrade pip
RUN pip install -r /project_tmp/requirements.txt
ENV NAME mongodb_rest_api





