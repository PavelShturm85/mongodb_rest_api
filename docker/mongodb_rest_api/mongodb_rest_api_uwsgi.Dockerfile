FROM python:3.7.4

WORKDIR /mongodb_rest_api

RUN mkdir mongodb_rest_api

ADD mongodb_rest_api /mongodb_rest_api/mongodb_rest_api

RUN apt-get update && apt-get upgrade -y && apt-get install -y  \
    --no-install-recommends apt-utils \
    build-essential libssl-dev libffi-dev

RUN mkdir /project_tmp

ADD req.txt /project_tmp/requirements.txt
ADD /conf/project_settings/local_settings.py /mongodb_rest_api/mongodb_rest_api/local_settings.py
ADD /conf/wsgi/uwsgi.ini /project_tmp/uwsgi.ini
RUN pip install --upgrade pip
RUN pip install -r /project_tmp/requirements.txt
RUN pip install --no-binary :all: falcon
RUN pip install -v --no-binary :all: falcon

ENV NAME mongodb_rest_api





