FROM nginx
ADD conf/nginx/nginx.conf /etc/nginx/nginx.conf
ADD conf/nginx/gunicorn.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
