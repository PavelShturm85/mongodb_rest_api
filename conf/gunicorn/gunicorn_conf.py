"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count

bind = "unix:/project_tmp/mongodb_rest_api/gunicorn.sock"
max_requests = 150000
workers = 3 * cpu_count() + 1
# threads = workers * 4
# worker_class = "gthread"

worker_class = "meinheld.gmeinheld.MeinheldWorker"

uid = "www-data"
gid = "www-data"
vacuum = True
