"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count

vacuum = True
bind = "unix:/project_tmp/mongodb_rest_api/gunicorn.sock"

max_requests = 5000
max_requests_jitter = 500

timeout = 25
graceful_timeout = 25

workers = 8 * cpu_count() + 1
worker_class = "tornado"

uid = "www-data"
gid = "www-data"

