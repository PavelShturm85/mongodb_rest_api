from mongodb_rest_api.views import MongodbRestApi, Tokens
from mongodb_rest_api.settings import mongodb


def create_routes(api, db):
    [api.add_route(f'/{collection}', MongodbRestApi(db[collection], db.tokens)) for collection in
     mongodb["collections"]]
    api.add_route('/token', Tokens(db.tokens))
