import json
import falcon
from bson.objectid import ObjectId
from mongodb_rest_api.authorize import AuthorizeToken
from mongodb_rest_api.db_actions import mongodb_actions_dict


class MongodbRestApi(object):

    def __init__(self, collection, tokens):
        self.tokens = tokens
        self.collection = collection
        self.mongodb_actions_dict = mongodb_actions_dict

    @falcon.before(AuthorizeToken())
    def on_get(self, req, resp):
        try:
            METHOD = "GET"
            req_query = req.get_param_as_json("query") or {}
            req_action = req.get_param("action")
            if not req_action:
                raise falcon.HTTPBadRequest("Not action.")
            req_sort = req.get_param_as_json("sort") or []
            req_limit = req.get_param_as_int("limit") or 100

            actions_for_current_method = self.mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                response_obj = action(self.collection, req_query, [req_sort, req_limit])
            else:
                raise falcon.HTTPBadRequest("Not support action.")
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_200
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_500

    @falcon.before(AuthorizeToken())
    def on_post(self, req, resp):
        try:
            METHOD = "POST"
            req_query = req.get_param_as_json("query")
            if not req_query:
                raise falcon.HTTPBadRequest("Not query for insert.")
            req_action = req.get_param("action")
            if not req_action:
                raise falcon.HTTPBadRequest("Not action.")

            actions_for_current_method = self.mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                action(self.collection, req_query)
            else:
                raise falcon.HTTPBadRequest("Not support action.")

            response_obj = {'status': 'success'}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_200
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_500

    @falcon.before(AuthorizeToken())
    def on_put(self, req, resp):
        try:
            METHOD = "PUT"
            req_query = req.get_param_as_json("query")
            if not req_query:
                raise falcon.HTTPBadRequest("Not query for update.")
            req_newvalues = req.get_param_as_json("newvalues")
            if not req_newvalues:
                raise falcon.HTTPBadRequest("Not new values for update.")
            req_action = req.get_param("action")
            if not req_action:
                raise falcon.HTTPBadRequest("Not action.")

            actions_for_current_method = self.mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                action(self.collection, req_query, req_newvalues)
            else:
                raise falcon.HTTPBadRequest("Not support action.")

            response_obj = {'status': 'success'}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_200
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_500

    @falcon.before(AuthorizeToken())
    def on_delete(self, req, resp):
        try:
            METHOD = "DELETE"
            req_query = req.get_param_as_json("query")
            if not req_query:
                raise falcon.HTTPBadRequest("Not query for delete.")
            req_action = req.get_param("action")
            if not req_action:
                raise falcon.HTTPBadRequest("Not action.")

            actions_for_current_method = self.mongodb_actions_dict.get(METHOD)
            action = actions_for_current_method.get(req_action)
            if action:
                action(self.collection, req_query)
            else:
                raise falcon.HTTPBadRequest("Not support action.")

            response_obj = {'status': 'success'}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_200
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_500


class Tokens(object):
    def __init__(self, tokens):
        self.tokens = tokens

    @falcon.before(AuthorizeToken())
    def on_delete(self, req, resp):
        try:
            req_query = req.get_param("id")
            req_action = req.get_param("action")
            if not req_action:
                raise falcon.HTTPBadRequest("Not action.")
            elif not req_action in "delete_token":
                raise falcon.HTTPBadRequest("Not support action.")
            else:
                token = self.tokens.find_one_and_delete({"_id": ObjectId(req_query)})
                if not token:
                    raise falcon.HTTPBadRequest("Not token in db.")

            response_obj = {'status': 'success'}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_200
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_500

    @falcon.before(AuthorizeToken())
    def on_post(self, req, resp):
        try:
            req_action = req.get_param("action")
            if not req_action:
                raise falcon.HTTPBadRequest("Not action.")
            elif not req_action in "get_token":
                raise falcon.HTTPBadRequest("Not support action.")
            else:
                new_token = self.tokens.save({"is_active": True, "type": "client"})

            response_obj = {"id": str(new_token)}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_200
        except Exception as e:
            response_obj = {'status': 'failed', 'reason': str(e)}
            resp.body = json.dumps(response_obj)
            resp.status = falcon.HTTP_500
