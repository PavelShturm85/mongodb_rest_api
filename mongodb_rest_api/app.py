import falcon
from mongodb_rest_api.routes import create_routes
from mongodb_rest_api.db_connect import init_mongo_db


def create_api():
    api = falcon.API()
    db = init_mongo_db()
    create_routes(api, db)
    return api
