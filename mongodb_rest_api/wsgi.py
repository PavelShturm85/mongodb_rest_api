import os
from mongodb_rest_api import app

# uWSGI will look for this variable
application = app.create_api()

# gunicorn mongodb_rest_api.wsgi --workers=6 --worker-class=meinheld.gmeinheld.MeinheldWorker --worker-connections=1000

# uwsgi src/uwsgi.ini
# gunicorn mongodb_rest_api.wsgi -c src/gunicorn_conf.py
