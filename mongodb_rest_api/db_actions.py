import falcon
from bson.objectid import ObjectId
from mongodb_rest_api.handler import objectId_to_str, str_to_objectId, correct_sort, check_instance


def find_one(collection: object, query: dict, args: list) -> dict:
    check_instance(query, dict)
    id = query.get("_id")
    if not id:
        raise falcon.HTTPBadRequest("Should use only \'_id\' key with \'find_one\' method.")

    return objectId_to_str(collection.find_one({"_id": ObjectId(id)}))


def find(collection: object, query: dict, args: list) -> list:
    check_instance(query, dict)
    sort, limit = args
    if sort and sort[0]:
        data = collection.find(str_to_objectId(query)).sort(correct_sort(sort))
    else:
        data = collection.find(str_to_objectId(query))
    return [objectId_to_str(obj) for obj in data.limit(int(limit))]


def insert_one(collection, query: dict):
    check_instance(query, dict)
    collection.insert_one(query)


def insert_many(collection, query: list):
    check_instance(query, list)
    collection.insert_many(query)


def delete_one(collection, query: dict):
    check_instance(query, dict)
    collection.delete_one(query)


def delete_many(collection, query: dict):
    check_instance(query, dict)
    collection.delete_many(query)


def update_one(collection, query: dict, new_values: dict):
    check_instance(query, dict)
    collection.update_one(query, new_values)


def update_many(collection, query: dict, new_values: dict):
    check_instance(query, dict)
    collection.update_many(query, new_values)


mongodb_actions_dict = dict(
    GET=dict(
        find_one=find_one,
        find=find,
    ),

    POST=dict(
        insert_one=insert_one,
        insert_many=insert_many,
    ),

    DELETE=dict(
        delete_one=delete_one,
        delete_many=delete_many,
    ),

    PUT=dict(
        update_one=update_one,
        update_many=update_many,
    ),

)
