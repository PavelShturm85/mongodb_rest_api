from pymongo import MongoClient
from mongodb_rest_api.settings import mongodb


def init_mongo_db():
    # https://www.w3schools.com/python/python_mongodb_getstarted.asp
    client = MongoClient(mongodb['ip'], mongodb['port'], connect=False)
    db = client[mongodb['db_name']]
    return db
