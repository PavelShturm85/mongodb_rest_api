import falcon
from bson.objectid import ObjectId


class AuthorizeToken(object):

    def __auth_basic(self, token, tokens):
        is_token = bool(tokens.find_one({"_id": ObjectId(token)}))
        if not is_token:
            raise falcon.HTTPUnauthorized("Unauthorized", "Your access is not allowed")

    def __call__(self, req, resp, resource, params):
        token = req.get_param('token')

        if token:
            tokens = resource.tokens
            self.__auth_basic(token, tokens)
        else:
            raise falcon.HTTPNotImplemented("Not implement", "You don\'t use auth token")
