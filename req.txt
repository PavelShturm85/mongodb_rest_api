Cython==0.29.11
falcon==2.0.0
pymongo==3.8.0
uWSGI==2.0.18
meinheld==1.0.1
gunicorn==19.9.0