"""gunicorn WSGI server configuration."""
from multiprocessing import cpu_count

bind = '0.0.0.0:' + '3030'
max_requests = 250000
workers = 8 * cpu_count() + 1

# threads = workers * 4
# worker_class = "gthread"

worker_class = "meinheld.gmeinheld.MeinheldWorker"

uid = "www-data"
gid = "www-data"
vacuum = True
